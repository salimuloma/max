#include <stdio.h>

int main(){
    // declare the array
    int myArray[6]={1,3,4,5,6,11};

    int max=myArray[0]; // assume the largest number is the first element in the array

    for (int i=1; i<6; i++){
        if(myArray[i]>max){
            max=myArray[i];
        }
    }
    printf("the maximum number is: %d\n", max);
    return 0;
}